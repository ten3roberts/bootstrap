#!/bin/sh
REPO_CONFIG="git@gitlab.com:ten3roberts/config"
REPO_SCRIPTS="git@gitlab.com:ten3roberts/scripts"
REPO_DWM="git@gitlab.com:ten3roberts/dwm"
REPO_DWMBLOCKS="git@gitlab.com:ten3roberts/dwmblocks"
REPO_ST="git@gitlab.com:ten3roberts/st"
REPO_NEOVIM="https://github.com/neovim/neovim"
REPO_TELA="https://github.com/vinceliuice/Tela-icon-theme"
REPO_MATCHA="https://github.com/vinceliuice/Matcha-gtk-theme"

REPO_NVIMCONFIG="git@gitlab.com:ten3roberts/nvim"

NEW_SHELL="/usr/bin/zsh"

DEB_PACKAGES="build-essential cmake git curl vim zsh tar unzip pkg-config psmisc tree htop ripgrep fzf herbstluftwm xinit libx11-dev rofi picom thunar tumbler thunar-volman thunar-archive-plugin wmctrl xdotool xtitle firefox-esr ristretto lxappearance x11-xserver-utils blender gimp inkscape breeze-cursor-theme"

VOID_PACKAGES="build-essential cmake git curl vim zsh tar unzip pkg-config psmisc tree htop ripgrep fzf herbstluftwm xorg-minimal libX11-devel rofi picom Thunar tumbler thunar-volman thunar-archive-plugin wmctrl xdotool xtitle pamixer firefox-esr ristretto lxappearance xrandr blender gimp inkscape breeze-cursor-theme"

show_help() {
    echo "This is a script that bootstraps a minimal installation with tools and personal configs"
    echo "Will clone and create directories in \$HOME and requires sudo for installing programs and source builds"
    echo "Will not delete any files nor force pull"
    echo "Works best on voidlinux and debian.\n"

    echo "Options:"
    echo "  --nossh - Use https for repositories"
    echo "  --abort - Abort on non-zero exit code"
    echo "  --nopkg - Do not install any packages with package manager (debian and void)"
    echo "  --noshell - Don't change default shell"
    echo "  --help - Shows this help menu"
}

while test $# -gt 0
do
    case "$1" in
        --nossh) NOSSH=true
            ;;
        --abort) ABORT=true
            ;;
        --nopkg) NOPKG=true
            ;;
        --help) show_help; exit
            ;;
        --noshell) NOSHELL=true
            ;;
        --*) echo "bad option $1"
            ;;
    esac
    shift
done

clone_or_pull() {
    REPO="$1"

    # Convert ssh to https links if set
    [ -n "$NOSSH" ] && REPO=`echo $REPO | sed s-:-/- | sed s-git@-https://-`

    mkdir -p `dirname $2`
    [ -d "$2" ]  \
        && (cd $2  \
        ; echo "Pulling into `pwd`" \
        ; git pull ) \
        || git clone "$REPO" "$2" 
    }


install_packages() {
    echo "Installing packages"
    type apt >/dev/null 2>&1 && DISTRO="debian"
    type xbps-install >/dev/null 2>&1 && DISTRO="void"

    [ -z "$DISTRO" ] && echo "  ERROR: Found no supported package manager" && exit -2

    echo Detected package manager for $DISTRO

    [ "$DISTRO" = "debian" ] && sudo apt update && sudo apt upgrade && sudo apt install $DEB_PACKAGES
    [ "$DISTRO" = "void" ] && sudo xbps-install -Su && sudo xbps-install $VOID_PACKAGES 
}

install_suckless() {
    echo "Installing $2"
    clone_or_pull $1 $2
    cd $2
    pwd
    echo "Building `pwd`"
    sudo make clean install
}

# Clone custom configs
install_dots() {
    echo "Installing dots"
    cd $HOME
    clone_or_pull $REPO_CONFIG $HOME/.config

    # Setup symlinks
    ln -f $HOME/.config/zsh/.zprofile $HOME
    ln -f $HOME/.config/.xsessionrc $HOME

    clone_or_pull $REPO_NVIMCONFIG $HOME/.config/nvim

    clone_or_pull $REPO_SCRIPTS $HOME/.scripts

    # Setup zsh plugins
    clone_or_pull https://github.com/zsh-users/zsh-autosuggestions $HOME/.config/zsh/zsh-autosuggestions
    clone_or_pull https://github.com/zsh-users/zsh-syntax-highlighting $HOME/.config/zsh/zsh-syntax-highlighting
}

install_themes() {
    echo "Installing themes"
    mkdir -p $HOME/src/themes
    cd $HOME/src/themes
    clone_or_pull $REPO_MATCHA Matcha-gtk-theme
    cd Matcha-gtk-theme
    ./install.sh > /dev/null
    cd ..

    clone_or_pull $REPO_TELA Tela-icon-theme
    cd Tela-icon-theme
    ./install.sh > /dev/null
    cd ..

    cd $HOME
}

install_font() {
    FONTNAME=`basename "$1" | cut -f1 -d'.' | tr -d ' '`

    [ -d "$HOME/.local/share/fonts/$FONTNAME" ] && echo "Font $FONTNAME already installed" && return

    echo "Installing font $FONTNAME"
    curl "$1" -Lso "$FONTNAME.zip"
    mkdir -p "$HOME/.local/share/fonts/$FONTNAME"
    unzip -qo "$FONTNAME.zip" -d "$HOME/.local/share/fonts/$FONTNAME"
    rm "$FONTNAME.zip"
}

install_fonts() {
    install_font "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Mononoki.zip" &

    install_font "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/RobotoMono.zip" &

    install_font "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip" &

    wait

    echo "Updating fonts cache"

    fc-cache -f
}

install_rust() {
    echo "Installing rust"
    type rustup > /dev/null && rustup update || curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
}

install_rust_tools() {
    echo "Installing rust tools"
    cargo install proximity-sort > /dev/null 
    cargo install bat > /dev/null 
    cargo install exa > /dev/null 
}

install_nim() {
    curl https://nim-lang.org/choosenim/init.sh -sSf | sh
}

install_go() {
    echo "Installing go"
    curl -sL https://golang.org/dl/go1.15.6.linux-amd64.tar.gz \
        | sudo tar -C /usr/local -xz
    }

install_neovim() {
    echo "Installing neovim"
    DEST="$HOME/src/neovim"
    clone_or_pull $REPO_NEOVIM $DEST
    cd $DEST

    make distclean
    echo "Building `pwd`"
    make CMAKE_BUILD_TYPE=RelWithDebInfo
    sudo make install > /dev/null

    # Install plug
    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    }

install_misc() {
    # For battery status bars
    sudo sh -c "curl https://raw.githubusercontent.com/holman/spark/master/spark -o /usr/local/bin/spark && chmod +x /usr/local/bin/spark"

    pip install pywal
}

[ -n "$ABORT" ] && set -e
# Don't exit on failure when installing packages due to apt update returning failure state when there is nothing to do
# Make sure all programs are installed
[ -z "$NOPKG" ] && install_packages

# Install and symlink dotfiles
install_dots
install_fonts
install_themes 

# Install from source
install_suckless $REPO_DWM $HOME/src/dwm
install_suckless $REPO_ST $HOME/src/st
install_suckless $REPO_DWMBLOCKS $HOME/src/dwmblocks

# install_rust
install_rust_tools 
install_go 

install_nim

install_neovim 

install_misc 

wait

[ -z "$NOSHELL" ] && [ "$SHELL" != "$NEW_SHELL" ] && echo "Changing shell to $NEW_SHELL" && chsh -s "$NEW_SHELL"

echo "It is now recommended to reboot your computer"
