# Bootstrap

This script will bootstrap a base debian testing or void linux installation.
Gets it up and running with all my tools and personal configuration.

Script can be run again to update everything; it won't overwrite or force pull
configs and will rebuild projects if they already exist.

## Usage
```shell
curl -L https://gitlab.com/ten3roberts/bootstrap/-/raw/master/bootstrap.sh | sh
```
Requires curl, all other dependencies will be installed by either apt or xbps.

If using other distribution than debian or void, dependencies and tools need to
be installed manually

## Options
| Option    | Description                                                        |
| --------- | ------------------------------------------------------------------ |
| --nossh   | Use https for repositories                                         |
| --abort   | Abort on non-zero exit code                                        |
| --nopkg   | Do not install any packages with package manager (debian and void) |
| --noshell | Don't change default shell                                         |
